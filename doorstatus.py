#!/usr/bin/python

# This continuously monitors the status of pin 23
# when it changes, it writes the state to a JSON file
# and logs the status
# 
# If its been open for more than 5mins, it calls
# the doornotify script
#
# Put this in /etc/rc.local to run boot as sudo
#     # Start garagedoor status program
#     sudo python /home/pi/scripts/doorstatus.py

import RPi.GPIO as GPIO
import time
import requests

# BCM nunbering
GPIO.setmode(GPIO.BCM)
closedsensorPin = 23

# Set pin as output
GPIO.setup(closedsensorPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

doorState = 'open'
previousState = 'new'
currenttime = int(time.time())
openedtime = currenttime
secondsTillWarn = 300 # 300 = 5mins
sentWarning = False

while True:
	input_state = GPIO.input(closedsensorPin)
	currenttime = int(time.time())
	if input_state == False:
		doorState = 'closed'
		openedtime = 4102444800
		sentWarning = False
	else:
		doorState = 'open'
	if doorState != previousState:
		# write status to json file
		log = open('/home/pi/garagedata/doorstatus.json','w')
		log.write('{"status":"' + doorState + '"}')
		log.close()
		
		# log the status change
		payload = {'status':doorState}
		r = requests.get('https://garagedoorbot/garagedoor/logDoorStatus',params=payload,verify=False)
		
		if doorState == 'open':
			openedtime = currenttime
	if doorState == 'open':
		if currenttime > openedtime+secondsTillWarn:
			# door has been open too long
			if sentWarning == False:
				# Send the warning
				# call doornotify.py
				execfile("/home/pi/scripts/doornotify.py")
				sentWarning = True
	previousState = doorState
	time.sleep(0.25)

