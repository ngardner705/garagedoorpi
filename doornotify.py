#!/usr/bin/python

# This script sends an email to me, with an attachment
# of the latest picture from the garage

import sendgrid
from garagedoorconfig import *
import time
import base64

# base64 encode the latest.jpg image
with open("/home/pi/garagedata/images/latest.jpg", "rb") as image_file:
    encoded_image = base64.b64encode(image_file.read())

sg = sendgrid.SendGridAPIClient(apikey=SENDGRID_API_KEY)
data = {
  "personalizations": [
    {
      "to": [
        {
          "email": "ngardner705@gmail.com"
        }
      ],
      "subject": "Garage Door OPEN"
    }
  ],
  "from": {
    "email": "garagedoor@wobotapps.com"
  },
  "content": [
    {
      "type": "text/plain",
      "value": "Garage door has been open too long. See: https://garagedoor.wobotapps.com"
    }
  ],
  "attachments": [
    {
      "content": encoded_image, 
      "content_id": "ii_139db99fdb5c3704", 
      "disposition": "inline", 
      "filename": "garagedooropen.jpg", 
      "name": "garagedooropen", 
      "type": "jpg"
    }
  ],
}
response = sg.client.mail.send.post(request_body=data)

if(response.status_code == 202):
	print('Notification sent')
else:
	print('ERROR!!! Notification not sent!')
	print(response.status_code)
	print(response.body)
	print(response.headers)

