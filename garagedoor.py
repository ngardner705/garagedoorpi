#!/usr/bin/python  

# Turns pin 18 to HIGH for 1/2 a second
# connect a relay, and the relay to your garage door
# button
# USE AT YOUR OWN RISK

import RPi.GPIO as GPIO
from time import sleep

# BCM nunbering
GPIO.setmode(GPIO.BCM)

# Set pin as output
GPIO.setup(18,GPIO.OUT)

#Turn on for half second
GPIO.output(18, GPIO.HIGH)
sleep(0.5)
GPIO.output(18, GPIO.LOW)

#Exit
GPIO.cleanup()
