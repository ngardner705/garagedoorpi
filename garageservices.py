#!/bin/sh
SERVICE_CAMERA='/home/pi/scripts/camera.py'
SERVICE_DOOR='/home/pi/scripts/doorstatus.py'

if ps ax | grep -v grep | grep $SERVICE_CAMERA > /dev/null
then
    echo "Camera already running"
else
    echo "Camera is not running, starting..."
    sudo $SERVICE_CAMERA > /dev/null &
    echo "Camera started"
fi

if ps ax | grep -v grep | grep $SERVICE_DOOR > /dev/null
then
    echo "Door monitor already running"
else
    echo "Door monitor is not running, starting..."
    sudo $SERVICE_DOOR > /dev/null &
    echo "Door monitor started"
fi

