#!/usr/bin/python

# This takes a picture about every 2 seconds, and saves
# to latest.jpg, and copies it to a loopCount.jpg for
# future use
# 
# gets ran by crontab every minute
# * * * * * /home/pi/scripts/camera.py


import picamera
import time
from shutil import copyfile
import datetime as dt
from pytz import timezone

localTimezone = timezone('US/Central')

with picamera.PiCamera() as camera:
	photoCount = 0
	camera.resolution = (640,480)
	camera.annotate_background = picamera.Color('black')
	# warm up camera
	time.sleep(2)
	while(True):
		camera.annotate_text = dt.datetime.now(localTimezone).strftime('%Y-%m-%d %H:%M:%S')
		camera.capture('/home/pi/garagedata/images/latest.jpg')
		copyfile('/home/pi/garagedata/images/latest.jpg','/home/pi/garagedata/images/' + str(photoCount) + '.jpg')
		photoCount = photoCount + 1
		time.sleep(1.85)
		if photoCount > 100:
			photoCount = 0
